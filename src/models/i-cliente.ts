export interface ICliente{
    id:number;
    nome:string | null;
    telefone:string | null;
    email:string | null;
    cpf:string;
    cep:string | null;
}