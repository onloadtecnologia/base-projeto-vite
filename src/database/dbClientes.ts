import { ICliente } from "../models/i-cliente";

const DbClientes: ICliente[] = [
    {
        id: 1,
        nome: "alan",
        telefone: "21000000000",
        email: "alan@gmail",
        cpf:"000000000000",
        cep: "00000000"
    },
    {
        id: 2,
        nome: "jose",
        telefone: "21000000000",
        email: "jose@gmail",
        cpf:"111111111111",
        cep: "00000000"
    },
    {
        id: 3,
        nome: "mario",
        telefone: "21000000000",
        email: "mario@gmail",
        cpf:"333333333333",
        cep: "00000000"
    },
    {
        id: 4,
        nome: "hugo",
        telefone: "21000000000",
        email: "hugo@gmail",
        cpf:"444444444444",
        cep: "00000000"
    },
    {
        id: 5,
        nome: "paulo",
        telefone: "21000000000",
        email: "paulo@gmail",
        cpf:"555555555555",
        cep: "00000000"
    }

];


export default DbClientes; 